package id.ac.ui.cs.advprog.tutorial4.singleton.Core;
import static org.junit.jupiter.api.Assertions.assertEquals;

import id.ac.ui.cs.advprog.tutorial4.singleton.core.OrderDrink;
import org.junit.jupiter.api.BeforeEach;
import org.junit.jupiter.api.Test;
import org.junit.jupiter.api.extension.ExtendWith;
import org.mockito.InjectMocks;
import org.mockito.junit.jupiter.MockitoExtension;

@ExtendWith(MockitoExtension.class)
public class OrderDrinkTest {
    private Class<?> orderDrinkClass;

    @InjectMocks
    private OrderDrink orderDrink;

    @BeforeEach
    public void setup() throws Exception {
        orderDrinkClass = Class.forName(
                "id.ac.ui.cs.advprog.tutorial4.singleton.core.OrderDrink");
    }

    @Test
    public void testSingletonObject() {
        OrderDrink orderDrink = OrderDrink.getInstance();
        OrderDrink orderDrink2 = OrderDrink.getInstance();
        assertEquals(orderDrink, orderDrink2);
    }
}
