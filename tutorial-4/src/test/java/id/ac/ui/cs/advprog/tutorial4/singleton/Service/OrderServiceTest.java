package id.ac.ui.cs.advprog.tutorial4.singleton.Service;

import id.ac.ui.cs.advprog.tutorial4.singleton.core.OrderDrink;
import id.ac.ui.cs.advprog.tutorial4.singleton.core.OrderFood;
import id.ac.ui.cs.advprog.tutorial4.singleton.service.OrderService;
import id.ac.ui.cs.advprog.tutorial4.singleton.service.OrderServiceImpl;
import org.junit.jupiter.api.BeforeEach;
import org.junit.jupiter.api.Test;
import org.junit.jupiter.api.extension.ExtendWith;
import org.mockito.Mock;
import org.mockito.Mockito;
import org.mockito.junit.jupiter.MockitoExtension;

import java.lang.reflect.Method;
import java.lang.reflect.Modifier;
import static org.junit.jupiter.api.Assertions.*;

@ExtendWith(MockitoExtension.class)
public class OrderServiceTest {
    private Class<?> orderServiceImplClass;

    @BeforeEach
    public void setup() throws Exception {
        orderServiceImplClass = Class.forName(
                "id.ac.ui.cs.advprog.tutorial4.singleton.service.OrderServiceImpl");
    }

    @Test
    public void testOrderServiceHasOrderADrinkMethod() throws Exception {
        Method orderADrink = orderServiceImplClass.getDeclaredMethod("orderADrink", String.class);
        assertTrue(Modifier.isPublic(orderADrink.getModifiers()));
    }

    @Test
    public void testOrderServiceHasGetDrinksMethod() throws Exception {
        Method getDrink = orderServiceImplClass.getDeclaredMethod("getDrink");
        assertTrue(Modifier.isPublic(getDrink.getModifiers()));
    }

    @Test
    public void testOrderServiceHasOrderAFoodMethod() throws Exception {
        Method orderAFood = orderServiceImplClass.getDeclaredMethod("orderAFood", String.class);
        assertTrue(Modifier.isPublic(orderAFood.getModifiers()));
    }

    @Test
    public void testOrderServiceHasGetFoodsMethod() throws Exception {
        Method getFood = orderServiceImplClass.getDeclaredMethod("getFood");
        assertTrue(Modifier.isPublic(getFood.getModifiers()));
    }
}
