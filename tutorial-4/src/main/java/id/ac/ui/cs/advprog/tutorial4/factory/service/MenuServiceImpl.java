package id.ac.ui.cs.advprog.tutorial4.factory.service;

import id.ac.ui.cs.advprog.tutorial4.factory.core.ingridients.flavor.Flavor;
import id.ac.ui.cs.advprog.tutorial4.factory.core.ingridients.meat.Meat;
import id.ac.ui.cs.advprog.tutorial4.factory.core.ingridients.noodle.Noodle;
import id.ac.ui.cs.advprog.tutorial4.factory.core.ingridients.topping.Topping;
import id.ac.ui.cs.advprog.tutorial4.factory.core.menu.*;
import id.ac.ui.cs.advprog.tutorial4.factory.repository.MenuRepository;
import org.springframework.stereotype.Service;
import java.util.List;

@Service
public class    MenuServiceImpl implements MenuService {
    private MenuRepository repo;

    public MenuServiceImpl(MenuRepository repo){
        this.repo = repo;
        initRepo();
    }

    public MenuServiceImpl(){
        this(new MenuRepository());
    }

    public Menu createMenu(String name, String type){
        MenuFactory menuFactory = null;
        if(type.equals("LiyuanSoba")){
            menuFactory = new LiyuanSoba(name);
        } else if(type.equals("InuzumaRamen")){
            menuFactory = new InuzumaRamen(name);
        } else if(type.equals("MondoUdon")) {
            menuFactory = new MondoUdon(name);
        } else {
            menuFactory = new SnevnezhaShirataki(name);
        }

        Menu newMenu = createMenuFromMenuFactory(menuFactory,name);
        return repo.add(newMenu);
    }

    public Menu createMenuFromMenuFactory (MenuFactory menuFactory, String name){
        Noodle noodle = menuFactory.createNoodle();
        Meat meat = menuFactory.createMeat();
        Topping topping = menuFactory.createTopping();
        Flavor flavor = menuFactory.createFlavor();
        Menu newMenu = new Menu(name, noodle, meat, topping, flavor);

        return newMenu;
    }

    public List<Menu> getMenus(){
        return repo.getMenus();
    }

    private void initRepo(){
        createMenu("WanPlus Beef Mushroom Soba", "LiyuanSoba");
        createMenu("Bakufu Spicy Pork Ramen", "InuzumaRamen");
        createMenu("Good Hunter Cheese Chicken Udon", "MondoUdon");
        createMenu("Morepeko Flower Fish Shirataki", "SnevnezhaShirataki");
    }
}