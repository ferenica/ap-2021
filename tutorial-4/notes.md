### Lazy Instantiation
Dalam metode ini, objek dibuat hanya jika diperlukan. Ini dapat mencegah pemborosan sumber daya. Implementasi metode getInstance() diperlukan yang mengembalikan instance. Saat objek dibuat dengan dalam suatu metode, ini memastikan bahwa objek tidak akan dibuat sampai dan kecuali jika diperlukan. Instance dirahasiakan sehingga tidak ada yang dapat mengaksesnya secara langsung. Dapat digunakan dalam lingkungan single threaded karena, beberapa utas dapat merusak properti singleton karena mereka dapat mengakses metode get instance secara bersamaan dan membuat banyak objek.

<b> Kelebihan : <b>
- Objek dibuat hanya jika dibutuhkan. Ini dapat mengatasi mengatasi sumber daya dan pemborosan waktu CPU.
- Exception Handling memungkinkan dalam metode ini<br>

<b> Kekurangan : <b>
- Setiap kali kondisi nol harus diperiksa.
- instance tidak dapat diakses secara langsung.
- Dalam lingkungan multithread, itu dapat merusak properti singleton.


### Eager Instantiation
Menurut prinsip Eager Instantiation, objek harus dibuat terlebih dahulu dan harus siap digunakan. Metode ini adalah metode paling sederhana untuk membuat kelas tunggal. Metode ini dapat digunakan ketika program akan selalu menggunakan instance kelas ini.

<b>Kelebihan :<b>
- sangat mudah diterapkan<br>

<b>Kekurangan : <b>
- Dapat menyebabkan pemborosan sumber daya. Karena instance kelas selalu dibuat, baik diperlukan atau tidak.
- Exception Handling tidak memungkinkan

sumber: https://www.geeksforgeeks.org/java-singleton-design-pattern-practices-examples/
