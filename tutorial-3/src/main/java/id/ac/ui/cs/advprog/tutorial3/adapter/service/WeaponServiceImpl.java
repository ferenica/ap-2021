package id.ac.ui.cs.advprog.tutorial3.adapter.service;

import id.ac.ui.cs.advprog.tutorial3.adapter.core.bow.Bow;
import id.ac.ui.cs.advprog.tutorial3.adapter.core.spellbook.Spellbook;
import id.ac.ui.cs.advprog.tutorial3.adapter.core.weapon.Weapon;
import id.ac.ui.cs.advprog.tutorial3.adapter.core.weaponadapters.BowAdapter;
import id.ac.ui.cs.advprog.tutorial3.adapter.core.weaponadapters.SpellbookAdapter;
import id.ac.ui.cs.advprog.tutorial3.adapter.repository.BowRepository;
import id.ac.ui.cs.advprog.tutorial3.adapter.repository.LogRepository;
import id.ac.ui.cs.advprog.tutorial3.adapter.repository.SpellbookRepository;
import id.ac.ui.cs.advprog.tutorial3.adapter.repository.WeaponRepository;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

import java.util.List;

// TODO: Complete me. Modify this class as you see fit~
@Service
public class WeaponServiceImpl implements WeaponService {

    // feel free to include more repositories if you think it might help :)

    @Autowired
    private LogRepository logRepository;

    @Autowired
    private BowRepository bowRepository;

    @Autowired
    private SpellbookRepository spellbookRepository;

    @Autowired
    private WeaponRepository weaponRepository;


    // TODO: implement me
    @Override
    public List<Weapon> findAll() {
        List<Weapon> allWeapon = weaponRepository.findAll();
        List<Bow> bow = bowRepository.findAll();
        List<Spellbook> spellbook = spellbookRepository.findAll();

        for (Bow i : bow) {
            Weapon bowAdapter = new BowAdapter(i);
            allWeapon.add(bowAdapter);
        }

        for (Spellbook i : spellbook) {
            Weapon spellbookAdapter = new SpellbookAdapter(i);
            allWeapon.add(spellbookAdapter);
        }

        return allWeapon;
    }

    // TODO: implement me
    @Override
    public void attackWithWeapon(String weaponName, int attackType) {
        Weapon weapon = weaponRepository.findByAlias(weaponName);
        Bow bow = bowRepository.findByAlias(weaponName);
        Spellbook spellbook = spellbookRepository.findByAlias(weaponName);

        if (bow != null) {
            weapon = new BowAdapter(bow);
        } else if (spellbook != null) {
            weapon = new SpellbookAdapter(spellbook);
        }

        String log = weapon.getHolderName() + " attacked with " + weapon.getName();
        if (attackType == 0){
            log =  log + " (normal attack): " + weapon.normalAttack();
        }
        else{
            log = log + " (charged attack): " + weapon.chargedAttack();
        }
        logRepository.addLog(log);
        weaponRepository.save(weapon);
    }

    // TODO: implement me
    @Override
    public List<String> getAllLogs() {
        return logRepository.findAll();
    }
}
