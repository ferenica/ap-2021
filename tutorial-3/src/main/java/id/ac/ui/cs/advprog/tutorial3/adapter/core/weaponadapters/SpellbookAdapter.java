package id.ac.ui.cs.advprog.tutorial3.adapter.core.weaponadapters;

import id.ac.ui.cs.advprog.tutorial3.adapter.core.spellbook.Spellbook;
import id.ac.ui.cs.advprog.tutorial3.adapter.core.weapon.Weapon;

// TODO: complete me :)
public class SpellbookAdapter implements Weapon {

    private Spellbook spellbook;
    private boolean isAlreadyAttack;

    public SpellbookAdapter(Spellbook spellbook) {
        this.spellbook = spellbook;
        this.isAlreadyAttack = false;
    }

    @Override
    public String normalAttack() {
        this.isAlreadyAttack = false;
        return spellbook.smallSpell();
    }

    @Override
    public String chargedAttack() {
        if (!isAlreadyAttack) {
            this.isAlreadyAttack = true;
            return spellbook.largeSpell();
        }
        return "can not charged attack!";
    }

    @Override
    public String getName() {
        return spellbook.getName();
    }

    @Override
    public String getHolderName() {
        return spellbook.getHolderName();
    }
}
